import os
import pandas as pd
import numpy as np
from plotly import graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px


# POSITIONAL LIMITS
GPS_ABSOLUTE_MIN = 0 # feet || RESET IN THE FUNCTION BASED ON MINIMUM ALTITUDE
# The following are with respect to the altitude of the airstrip which is set in each function
GPS_PATTERN_ALTITUDE = 1300 + GPS_ABSOLUTE_MIN # feet
GPS_OBSTACLE_CLEARANCE_ALTITUDE = 50 + GPS_ABSOLUTE_MIN # feet

# VELOCITY LIMITS
AIRSPEED_TAKEOFF_MAX = 90 # knots
AIRSPEED_TAKEOFF_MIN = 85 # knots
AIRSPEED_TAXI_MAX = 20 # knots
AIRSPEED_TAXI_MIN = 6 # knots
CLIMB_RATE_MIN = 1.016 # FPM


test_set_config_dictionary = {
    "20210611_whole" : {'mass': "4500"},
    "20210625_whole" : {'mass':  "4500"},
    "20210630_whole" : {'mass':  "4500"},
    "20210701_whole" : {'mass': "4500"},
    "20210707_whole" : {'mass':  "5110"},
    "20210715_whole" : {'mass':  "5500"},
    "20210720_whole" : {'mass':  "4500"}, 
    "20210722_whole" : {'mass':  "4500"},
}

def import_resample_and_label_time(path, usecols, resample_width):

    df = pd.read_csv(path, usecols=usecols)

    ## Convert timestrings under column 'Timestamp' to datetime64 under 'Datetime' and seconds elapsed under 'Elapsed'
    df['Datetime'] = pd.to_datetime(df['Timestamp'], format='%H:%M:%S:%f')
    df.set_index('Datetime', inplace=True)

    ## Resample data to a 1s period using the (mean) of each period
    period_string = str(resample_width) + 's'
    df = df.resample(period_string).mean()
    df['Elapsed'] = (df.index - pd.Timestamp("1970-01-01")) // pd.Timedelta('1ns')
    df['Elapsed'] = (df['Elapsed'] - df['Elapsed'].iloc[0]) / 10**9

    return ( df )

def takeoff_ground_roll_points(path, set) -> pd.DataFrame:
    """Calculate Ground Roll Distance and times for specific Pattern points.
    Args:
        pattern_points (pd.DataFrame): dataframe flight points below pattern
    Returns:
        pd.DataFrame: dataframe of just L/D points with L/D as a column
    """
    index = ['Timestamp']

    ## Pull in only the necessary and relevant variables from the CSVs to save on compute time

    velocities = ['True Airspeed', 'Airspeed', 'Ground Speed', 'Climb Rate', 'Angle of Attack']
    positions = ['GPS Altitude']
    angles = ['ECU Roll Angle', 'ECU Pitch Angle', 'ECU Yaw Angle']
    latlong = ['Latitude', 'Longitude']
    epu = ['Pusher EPU A Commanded Torque' , 'Pusher EPU B Commanded Torque']

    ## Define sets of the variables depending on where they are going

    usecols = index + velocities + positions + angles + latlong + epu

    differentiables = velocities + positions + angles


    ## Define all usable dataframes:

    storage_columns = ["Date","Weight","Roll Distance of Point","Roll Time of Point"]


    takeoff_roll_data = pd.DataFrame(columns = storage_columns)

    ## Read in CSV to DataFrame
    RESAMPLE_WIDTH = 1  ## INTEGER SECONDS
    test_points = import_resample_and_label_time(path, usecols, RESAMPLE_WIDTH)

    ## Primary filter to remove discontinuities like zeros or NaNs from the dataframe
    GPS_ABSOLUTE_MIN = test_points['GPS Altitude'].min() 

    gps_cutoff = test_points['GPS Altitude'] > GPS_ABSOLUTE_MIN

    test_points = test_points[(gps_cutoff)]
    # test_points = test_points[(gps_cutoff)]

    ## Secondary filter to cut the data frame to just sections below takeoff speed but above taxi speed
    GPS_AIRSTRIP_ALTITUDE = test_points['GPS Altitude'].min()  ## Set new minimum to be the airstrip altitude
    TEST_LENGTH_SECONDS = test_total_time = test_points['Elapsed'].max()

    ## DEFINE CONSTANTS OF MARGINS

    GPS_GROUNDROLL_MAX = GPS_AIRSTRIP_ALTITUDE + 20
    GPS_GROUNDROLL_MIN = GPS_AIRSTRIP_ALTITUDE - 20

    ## TRANSLATE INTO FILTER ARGUMENTS

    gps_upper_groundroll = test_points['GPS Altitude'] < GPS_GROUNDROLL_MAX
    gps_lower_groundroll = test_points['GPS Altitude'] > GPS_GROUNDROLL_MIN 
    true_airspeed_upper = test_points['True Airspeed'] < AIRSPEED_TAKEOFF_MIN
    true_airspeed_lower = test_points['True Airspeed'] > AIRSPEED_TAXI_MAX 
    time_elapsed_upper = test_points["Elapsed"] < (TEST_LENGTH_SECONDS)


    ## APPLY FILTER
    ground_roll_points = test_points[ (time_elapsed_upper) & (gps_upper_groundroll) & (gps_lower_groundroll) & (true_airspeed_upper) & (true_airspeed_lower)]

    ## USE TIME DIFFERENCING TO DIFFERENTIATE DISTINCT SECTIONS OF FILTERED DATA
    ground_roll_points = ground_roll_points.reset_index()
    ground_roll_points['Time Difference'] = ground_roll_points['Elapsed'].diff()

    ## CONSTANTS AND LISTS

    ground_speed_list = []
    seconds_elapsed_list = []
    air_density_list = []

    TIME_DIFFERENCE_MIN = 10

    test_total_time = ground_roll_points['Elapsed'].max()
    print("Total Time", test_total_time)

    WEIGHT = test_set_config_dictionary[set]['mass']

    for index, row in ground_roll_points.iterrows():
        ## RECORD ALL THE DATA IN ONE DISCREET SECTION
        if row['Time Difference'] <= TIME_DIFFERENCE_MIN:
            ground_speed_list.append(row['Ground Speed']*3.2808399)
            seconds_elapsed_list.append(row['Elapsed'])
            air_density_list.append( 1.225 / ((row['True Airspeed'] / row["Airspeed"])**2) )

        ## IF A TIME DIFFERENCE BIG ENOUGH IS FOUND, STOP RECORDING ... PERFORM A CALCULATION
        if (len(seconds_elapsed_list) > 10) and (row['Time Difference'] > TIME_DIFFERENCE_MIN) :

            point_start_time = seconds_elapsed_list[0]
            point_end_time = seconds_elapsed_list[-1]
            point_time = (point_end_time - point_start_time)

            # Just incase some sneaky short rolls got in there...

            point_distance = np.trapz(ground_speed_list)
            point_start_time = seconds_elapsed_list[0]
            point_end_time = seconds_elapsed_list[-1]
            point_time = (point_end_time - point_start_time)
            average_density = ( 1.225 / ((row['True Airspeed'] / row["Airspeed"])**2) )
            average_density_altitude = ((-31204*np.average(air_density_list))+38074)
            print("POTENTIAL Takeoff GROUND ROLL - Distance", point_distance,"Time", point_time, "Elapsed start", seconds_elapsed_list[0] ,  "Elapsed start", seconds_elapsed_list[-1])
            
            ## SELECT THE MOST VIABLE GROUP OF DATA AS THE TRUE SET

            if  (500 > point_time > 10) and (8000 > point_distance > 1000) :

                print("SELECTED Takeoff GROUND ROLL - Distance", point_distance,"Time", point_time, "Elapsed start", seconds_elapsed_list[0] ,  "Elapsed start", seconds_elapsed_list[-1])
                
                takeoff_roll_data["Date"] = [set]
                takeoff_roll_data["Weight"] = [WEIGHT]
                takeoff_roll_data["Roll Distance of Point"] = [point_distance]
                takeoff_roll_data["Roll Time of Point"] = [point_time]
                takeoff_roll_data["Average Air Density of Point"] = [average_density]
                takeoff_roll_data["Average Density Altitude"] = [average_density_altitude]

            ## RESET POINT LISTS      
            ground_speed_list = [] 
            seconds_elapsed_list = []
            air_density_list = []

    return (ground_roll_points, takeoff_roll_data)


def landing_ground_roll_points(path,set) -> pd.DataFrame:
    """Calculate Ground Roll Distance and times for specific Pattern points.
    Args:
        pattern_points (pd.DataFrame): dataframe flight points below pattern
    Returns:
        pd.DataFrame: dataframe of just L/D points with L/D as a column
    """
     # variable setup

   
    index = ['Timestamp']

    ## Pull in only the necessary and relevant variables from the CSVs to save on compute time


    velocities = ['True Airspeed', 'Airspeed', 'Ground Speed', 'Climb Rate', 'Angle of Attack']
    positions = ['GPS Altitude']
    angles = ['ECU Roll Angle', 'ECU Pitch Angle', 'ECU Yaw Angle']
    latlong = ['Latitude', 'Longitude']
    epu = ['Pusher EPU A Commanded Torque' , 'Pusher EPU B Commanded Torque']


    ## Define sets of the variables depending on where they are going


    usecols = index + velocities + positions + angles + latlong + epu

    differentiables = velocities + positions + angles


    ## Define all usable dataframes:


    storage_columns = ["Date","Weight","Roll Distance of Point","Roll Time of Point"]


    landing_roll_data = pd.DataFrame(columns = storage_columns)


    ## Read in CSV to DataFrame

    RESAMPLE_WIDTH = 1  ## INTEGER SECONDS
    test_points = import_resample_and_label_time(path, usecols, RESAMPLE_WIDTH)

    ## Primary filter to remove discontinuities like zeros or NaNs from the dataframe
    GPS_ABSOLUTE_MIN = test_points['GPS Altitude'].min() 

    gps_cutoff = test_points['GPS Altitude'] > GPS_ABSOLUTE_MIN

    test_points = test_points[(gps_cutoff)]
    # test_points = test_points[(gps_cutoff)]

    ## Secondary filter to cut the data frame to just sections below takeoff speed but above taxi speed
    GPS_AIRSTRIP_ALTITUDE = test_points['GPS Altitude'].min()  ## Set new minimum to be the airstrip altitude
    TEST_LENGTH_SECONDS = test_total_time = test_points['Elapsed'].max()

    ## DEFINE CONSTANTS OF MARGINS

    GPS_GROUNDROLL_MAX = GPS_AIRSTRIP_ALTITUDE + 20
    GPS_GROUNDROLL_MIN = GPS_AIRSTRIP_ALTITUDE - 20

    ## TRANSLATE INTO FILTER ARGUMENTS

    gps_upper_groundroll = test_points['GPS Altitude'] < GPS_GROUNDROLL_MAX
    gps_lower_groundroll = test_points['GPS Altitude'] > GPS_GROUNDROLL_MIN 
    true_airspeed_upper = test_points['True Airspeed'] < AIRSPEED_TAKEOFF_MIN
    true_airspeed_lower = test_points['True Airspeed'] > AIRSPEED_TAXI_MAX 
    time_elapsed_upper = test_points["Elapsed"] < TEST_LENGTH_SECONDS
    time_elapsed_lower = test_points["Elapsed"] > (TEST_LENGTH_SECONDS * 0.5)

    ## APPLY FILTER
    ground_roll_points = test_points[ (time_elapsed_upper) & (time_elapsed_lower) & (gps_upper_groundroll) & (gps_lower_groundroll) & (true_airspeed_upper) & (true_airspeed_lower)]

    ## USE TIME DIFFERENCING TO DIFFERENTIATE DISTINCT SECTIONS OF FILTERED DATA
    ground_roll_points = ground_roll_points.reset_index()
    ground_roll_points['Time Difference'] = ground_roll_points['Elapsed'].diff()

    ## CONSTANTS AND LISTS


    ground_speed_list = []
    seconds_elapsed_list = []
    air_density_list = []

    TIME_DIFFERENCE_MIN = 10


    print("Total Time", test_total_time)
    has_found_point = False
    WEIGHT = test_set_config_dictionary[set]['mass']

    for index, row in ground_roll_points.iterrows():
        ## RECORD ALL THE DATA IN ONE DISCREET SECTION
        if row['Time Difference'] <= TIME_DIFFERENCE_MIN:
            ground_speed_list.append(row['Ground Speed']*3.2808399)
            seconds_elapsed_list.append(row['Elapsed'])
            air_density_list.append( 1.225 / ((row['True Airspeed'] / row["Airspeed"])**2) )

        ## IF A TIME DIFFERENCE BIG ENOUGH IS FOUND, STOP RECORDING ... PERFORM A CALCULATION
        if (len(seconds_elapsed_list) > 20) and (row['Time Difference'] > TIME_DIFFERENCE_MIN) or (index == ground_roll_points.index[-1]):

            point_start_time = seconds_elapsed_list[0]
            point_end_time = seconds_elapsed_list[-1]
            point_time = (point_end_time - point_start_time)

            # Just incase some sneaky short rolls got in there...

            point_distance = np.trapz(ground_speed_list)
            point_start_time = seconds_elapsed_list[0]
            point_end_time = seconds_elapsed_list[-1]
            point_time = (point_end_time - point_start_time)
            average_density = ( 1.225 / ((row['True Airspeed'] / row["Airspeed"])**2) )
            average_density_altitude = ((-31204*np.average(air_density_list))+38074)
            print("POTENTIAL LANDING ROLL - Distance", point_distance,"Time", point_time, "Elapsed start", seconds_elapsed_list[0] ,  "Elapsed end", point_end_time)
            
            ## SELECT THE MOST VIABLE GROUP OF DATA AS THE TRUE SET
            if ( 400 > point_time > 20) and ( 12000 > point_distance > 2000):

                print("SELECTED LANDING ROLL - Distance", point_distance,"Time", point_time, "Elapsed start", seconds_elapsed_list[0] ,  "Elapsed start", seconds_elapsed_list[-1])

                landing_roll_data["Date"] = [set]
                landing_roll_data["Weight"] = [WEIGHT]
                landing_roll_data["Roll Distance of Point"] = [point_distance]
                landing_roll_data["Roll Time of Point"] = [point_time]
                landing_roll_data["Average Air Density of Point"] = [average_density]
                landing_roll_data["Average Density Altitude"] = [average_density_altitude]
        
            ## RESET POINT LISTS      
            ground_speed_list = [] 
            seconds_elapsed_list = []
            air_density_list = []

    return (ground_roll_points, landing_roll_data)



############
## MAIN
###########

## ADD A SERIES DATA PARSING FUNCTIONS (above) AND FEED ALIA 250 CSV FORMAT FLIGHT TEST DATA INTO IT


csv_folder_path = os.path.dirname(os.path.realpath(__file__)) + "/usable_csvs"


# List of files in the csv directory


csv_file_name_list = os.listdir(csv_folder_path)

print( "Found " + str(len(csv_file_name_list)) + " tests")


nameout = "takeoff_and_landing_data.csv"

set_list = []
landing_roll_data_list = []
takeoff_roll_data_list = []

fig7 = make_subplots(rows=1, cols=1, x_title = "Climbrate vs Torque Setting 30% - 100 %")
fig8 = make_subplots(rows=1, cols=1, x_title = "Climbrate vs Density Altitude 30% - 100 %")

altitude = np.arange(500 , 7000, 250 )
torque = np.arange(0.3 , 1 , 0.1 )
for set in csv_file_name_list:

    print("Reading in", set, "..." )


    # obstacle_clearance_points_output = obstacle_clearance_points(pattern_points_output[0])
    takeoff_ground_roll_points_output = takeoff_ground_roll_points(csv_folder_path + "/" +set,set)
    landing_ground_roll_points_output = landing_ground_roll_points(csv_folder_path + "/" +set,set)
    print(takeoff_ground_roll_points_output[1])
    takeoff_roll_data_list.append(takeoff_ground_roll_points_output[1])
    landing_roll_data_list.append(landing_ground_roll_points_output[1])

takeoff_roll_data = pd.concat(takeoff_roll_data_list)    
landing_roll_data = pd.concat(landing_roll_data_list)    

print(takeoff_roll_data)


print(takeoff_roll_data["Weight"])

fig7.add_trace(go.Scatter(x=takeoff_roll_data["Date"][:8], y=takeoff_roll_data["Roll Distance of Point"],mode='markers'), row=1, col=1)

fig8.add_trace(go.Scatter(x=landing_roll_data["Date"][:8], y=landing_roll_data["Roll Distance of Point"], mode='markers'), row=1, col=1)

fig7.show()
fig8.show()

# fig6 = make_subplots(rows=1, cols=1)
# fig6.add_trace(go.Scatter(x=low_torque_climb_rates_total["Average Air Density of Point"], y=low_torque_climb_rates_total["Average Climb Rate of Point"], mode='markers', name='Low Torque Climbs', color = low_torque_climb_rates_total["Date"]), row=1, col=1)
# fig6.add_trace(go.Scatter(x=high_torque_climb_rates_total["Average Air Density of Point"], y=high_torque_climb_rates_total["Average Climb Rate of Point"], mode='markers', name='High Torque Climbs',color =  high_torque_climb_rates_total["Date"]), row=1, col=1)
# fig6.show()

output_dataframe = pd.DataFrame()


set_list.append(set)